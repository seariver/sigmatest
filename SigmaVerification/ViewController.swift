//
//  ViewController.swift
//  SigmaVerification
//
//  Created by Macbook Pro on 6/5/20.
//  Copyright © 2020 Macbook Pro. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var dataArray = [String]()
        
    var completionBlock: (() -> Void)?
    
    var locationManager: CLLocationManager?
    
    var postQueue: DispatchQueue?
    
    var getBatteryLevelTimer: DispatchSource?
    
    var updateLocationTimer: DispatchSource?
    
    var concurrentQueue: DispatchQueue?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.isBatteryMonitoringEnabled = true
        createCompletionBlock()
        initLocationManager()
    }
    
    /// Create post queue to post data
    func createCompletionBlock() {
        completionBlock = {
            self.concurrentQueue?.async(flags: .barrier) {
                if self.dataArray.count > 5 {
                    guard let url = URL(string: "http://sigma-solutions.eu/test") else {
                        return
                    }
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST"
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                    request.httpBody = try! JSONSerialization.data(withJSONObject: self.dataArray)
                    AF.request(request)
                        .responseJSON { response in
                            switch response.result {
                            case .failure(let error):
                                print(error)
                                
                                if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                                    print(responseString)
                                }
                            case .success(let responseObject):
                                print(responseObject)
                            }
                    }
                }
            }
        }
    }
    
    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
    }
    
    @IBAction func btnStartTapped(_ sender: Any) {
        if concurrentQueue == nil {
            concurrentQueue = DispatchQueue(label: "Concurrent Queue", attributes: .concurrent)
        }
        getBatteryLevelTimer?.cancel()
        /// Use getBatteryLevelTimer to scheduled get battery level in 1 thread
        getBatteryLevelTimer = DispatchSource.makeTimerSource(flags: .init(), queue: concurrentQueue) as? DispatchSource
        getBatteryLevelTimer?.schedule(deadline: .now(), repeating: .seconds(6 * 60), leeway: .milliseconds(100))
        getBatteryLevelTimer?.setEventHandler(handler: {[weak self] in
            guard let self = self else { return }
            self.concurrentQueue?.async(flags: .barrier) {
                self.dataArray.append("Battery level: \(UIDevice.current.batteryLevel)%")
                self.completionBlock?()
            }
        })
        getBatteryLevelTimer?.resume()
        
        updateLocationTimer?.cancel()
        /// Use updateLocationTimer to scheduled update location in in 1 thread
        updateLocationTimer = DispatchSource.makeTimerSource(flags: .init(), queue: concurrentQueue) as? DispatchSource
        updateLocationTimer?.schedule(deadline: .now(), repeating: .seconds(9 * 60), leeway: .milliseconds(100))
        updateLocationTimer?.setEventHandler(handler: {[weak self] in
            guard let self = self else { return }
            self.locationManager?.startUpdatingLocation()
        })
        updateLocationTimer?.resume()
    }
    
    @IBAction func btnStopTapped(_ sender: Any) {
        /// Stop all timer
        getBatteryLevelTimer?.cancel()
        updateLocationTimer?.cancel()
        concurrentQueue?.async(flags: .barrier) {
            self.dataArray.removeAll()
        }
        concurrentQueue?.suspend()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        guard let location = locations.last else {
            return
        }
        self.concurrentQueue?.async(flags: .barrier) {
            let locationString = "Coordinate: (\(location.coordinate.longitude),\(location.coordinate.latitude))"
            self.dataArray.append(locationString)
            self.completionBlock?()
        }
        return
    }
    
}
